﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Microsoft.Win32;

namespace xLightsBatchRenderer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = ShowDirectory.Text;

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                ShowDirectory.Text = folderBrowserDialog1.SelectedPath;
                LoadSequences();
            }
        }

        void DoEnable(bool e)
        {
            buttonRender.Enabled = e;
            buttonBrowseShowDirectory.Enabled = e;
            buttonClose.Enabled = e;
            parallelRender.Enabled = e;
            checkedListBox1.Enabled = e;
            checkBox_ScanSubdirectories.Enabled = e;
        }

        private void buttonRender_Click(object sender, EventArgs e)
        {
            string tempFile = Path.GetTempFileName();
            StreamWriter sw = new StreamWriter(tempFile);

            DoEnable(false);

            List<Process> processes = new List<Process>();
            Dictionary<int, string> map = new Dictionary<int, string>();

            if (!parallelRender.Checked)
            {
                progressBar1.Minimum = 0;
                progressBar1.Maximum = checkedListBox1.CheckedItems.Count;
                progressBar1.Value = 0;

                // this isnt working so keep it hidden for now
                //progressBar1.Show();
            }

            foreach (var i in checkedListBox1.CheckedItems)
            {
                string fseq = ShowDirectory.Text + "\\" + i as string;
                if (fseq.ToLower().EndsWith(".xml"))
                {
                    fseq = fseq.Substring(0, fseq.Length - 3);
                    fseq += "fseq";

                    if (File.Exists(fseq))
                    {
                        File.Delete(fseq);
                    }
                }

                Process p = Process.Start("xlights.exe",
                    "/s \"" + ShowDirectory.Text + "\" /r \"" + ShowDirectory.Text + "\\" + i as string + "\"");
                map[p.Id] = ShowDirectory.Text + "\\" + i as string;

                if (parallelRender.Checked)
                {
                    processes.Add(p);
                }
                else
                {
                    p.WaitForExit();
                    progressBar1.Value = progressBar1.Value + 1;
                    Refresh();
                    System.Threading.Thread.Yield();
                    sw.WriteLine("{0},{1:yyyy-MM-dd HH:mm:ss},{2:yyyy-MM-dd HH:mm:ss},{3}",
                        map[p.Id],
                        p.StartTime,
                        p.ExitTime,
                        (p.ExitTime - p.StartTime).TotalSeconds);
                }
            }

            if (parallelRender.Checked)
            {
                foreach (var p in processes)
                {
                    p.WaitForExit();
                    sw.WriteLine("{0},{1:yyyy-MM-dd HH:mm:ss},{2:yyyy-MM-dd HH:mm:ss},{3}",
                        map[p.Id],
                        p.StartTime,
                        p.ExitTime,
                        (p.ExitTime - p.StartTime).TotalSeconds);
                }
            }

            sw.Close();

            MessageBox.Show(this, @"Rendering done.");

            progressBar1.Hide();

            Process.Start(tempFile);

            DoEnable(true);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // read the xlights show directory
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Xlights");
            if (key != null)
            {
                Object o = key.GetValue("LastDir");
                if (o != null)
                {
                    ShowDirectory.Text = o as String;
                }
            }
            LoadProfile();
            LoadWindowPos();
            LoadSequences();
            LoadProfile(); // reload profile to ensure sequences are selected
            ValidateWindow();
        }

        void LoadSequences()
        {
            checkedListBox1.Items.Clear();

            if (Directory.Exists(ShowDirectory.Text))
            {
                LoadDirectory(ShowDirectory.Text);
            }
            ValidateWindow();
        }

        void LoadDirectory(string dir)
        {
            DirectoryInfo di = new DirectoryInfo(dir);
            if (checkBox_ScanSubdirectories.Checked)
            {
                foreach (DirectoryInfo d in di.GetDirectories())
                {
                    if (d.Name != "Backup")
                    {
                        LoadDirectory(d.FullName);
                    }
                }
            }

            foreach (FileInfo f in di.GetFiles("*.xml"))
            {
                if (IsSequence(f.FullName))
                {
                    checkedListBox1.Items.Add(f.FullName.Substring(ShowDirectory.Text.Length + 1), false);
                }
            }
        }

        bool IsSequence(string filename)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(filename);
                foreach (XmlNode n in doc.ChildNodes)
                {
                    if (n.Name == "xsequence") return true;
                }
            }
            catch (Exception e)
            {
            }
            return false;
        }

        void ValidateWindow()
        {
            if (Directory.Exists(ShowDirectory.Text) && checkedListBox1.CheckedItems.Count > 0)
            {
                buttonRender.Enabled = true;
            }
            else
            {
                buttonRender.Enabled = false;
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            ValidateWindow();
        }

        private void clearSelectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetSelections(false);
        }

        void SetSelections(bool select)
        {
            for (int ii = 0; ii < checkedListBox1.Items.Count; ii++)
            {
                checkedListBox1.SetItemChecked(ii, select);
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetSelections(true);
        }

        void SaveProfile()
        {
            List<string> items = new List<string>();
            foreach (var item in checkedListBox1.CheckedItems)
            {
                items.Add(item as string);
            }

            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Xlights", true);
            if (key != null)
            {
                key.SetValue("BatchRendererItems", items.ToArray(), RegistryValueKind.MultiString);
                key.SetValue("BatchRenderSubdirectories", checkBox_ScanSubdirectories.Checked, RegistryValueKind.DWord);
            }
        }

        void LoadProfile()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Xlights");
            if (key != null)
            {
                string[] values = (string[])key.GetValue("BatchRendererItems");

                if (values != null)
                {
                    foreach (string s in values)
                    {
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)
                        {
                            if (checkedListBox1.Items[i] as string == s)
                            {
                                checkedListBox1.SetItemChecked(i, true);
                                break;
                            }
                        }
                    }
                }

                if (key.GetValue("BatchRenderSubdirectories") != null)
                {
                    checkBox_ScanSubdirectories.Checked = (int)key.GetValue("BatchRenderSubdirectories") != 0;
                }
            }
        }

        void SaveWindowPos()
        {
            string pos = string.Format("{0},{1},{2},{3}", Location.X, Location.Y, Size.Width, Size.Height);
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Xlights", true);
            if (key != null)
            {
                key.SetValue("BatchRendererPos", pos, RegistryValueKind.String);
            }
        }

        bool isPointVisibleOnAScreen(Point p)
        {
            foreach (Screen s in Screen.AllScreens)
            {
                if (p.X < s.Bounds.Right && p.X > s.Bounds.Left && p.Y > s.Bounds.Top && p.Y < s.Bounds.Bottom)
                    return true;
            }
            return false;
        }

        void LoadWindowPos()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Xlights");
            if (key != null)
            {
                string pos = (string)key.GetValue("BatchRendererPos");
                if (pos != null)
                {
                    string[] items = pos.Split(new char[] {','});
                    if (items.Length == 4)
                    {
                        System.Drawing.Point pt = new Point
                        {
                            X = Int32.Parse(items[0]),
                            Y = Int32.Parse(items[1])
                        };

                        if (isPointVisibleOnAScreen(pt))
                        {
                            Location = pt;
                        }

                        Size sz = new Size
                        {
                            Width = Int32.Parse(items[2]),
                            Height = Int32.Parse(items[3])
                        };
                        Size = sz;
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveProfile();
            SaveWindowPos();
        }

        private void checkBox_ScanSubdirectories_CheckedChanged(object sender, EventArgs e)
        {
            LoadSequences();
            ValidateWindow();
        }
    }
}
